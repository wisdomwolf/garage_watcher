#!/usr/bin/env python

import paho.mqtt.client as paho
from pushbullet import Pushbullet
from apscheduler.schedulers.background import BackgroundScheduler
import logging
from logging.config import fileConfig
from configparser import ConfigParser, NoSectionError
from reminder import Reminder
import yaml
import jmespath
import os
import argparse


# try:
#     fileConfig('./config/logging_config.ini')
# except KeyError:
#     pass


def setup_logging(
        default_path='./config/logging_config.yaml',
        default_level=logging.INFO,
        env_key='LOG_CFG'
):
    """Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def watcher_update(client, userdata, msg):
    logger.info('new watcher command received: {}'.format(msg.payload))
    payload = msg.payload.decode('utf8')
    logger.info('retain: {}'.format(msg.retain))
    if payload.lower() == mqtt_start_command.lower():
        logger.warning('received {}. Activating reminder'.format(msg.payload))
        reminder.activate()
    if payload.lower() == mqtt_stop_command.lower():
        logger.warning('received {}.  Cancelling reminders'.format(msg.payload))
        reminder.cancel()
        notify_pushbullet(pb, 'Garage Watcher', 'Garage closed', 'wisehome')


def alert():
    logger.debug('alert triggered')
    if reminder.active:
        text = 'Garage door is open'
        if not args.dryrun:
            notify_pushbullet(pb, 'Garage Watcher', text, 'wisehome')
            logger.warning('push notification sent: {}'.format(text))
        else:
            logger.warning('would have sent: {}'.format(text))


def notify_pushbullet(pb, title, text, channel=None):
    """Pushbullet notification abstraction that allows selecting a channel by tag
    
    :param pb: Pushbullet object
    :type pb: Pushbullet
    :param title: Message Title
    :type title: String
    :param text: Message Text
    :type text: String
    :param channel: Channel Tag, defaults to None
    :param channel: String, optional
    :return: Pushbullet push return
    :rtype: Dictionary
    """


    if channel:
        channel_dict = {channel.channel_tag: channel for channel in pb.channels}
        target = channel_dict.get(channel) or pb
    else:
        target = pb
    return target.push_note(title, text)


def setup(config_path=None):
    global reminder
    global mqtt_start_command
    global mqtt_stop_command
    global pb
    config_path = config_path or './config/config.yml'
    with open(config_path) as f:
        config = yaml.load(f)

    # Pushbullet Setup
    pb_token = jmespath.search('pushbullet.token', config)
    pb = Pushbullet(pb_token)

    # MQTT Feed Configuration
    mqtt_host = jmespath.search('mqtt.host', config)
    mqtt_port = int(jmespath.search('mqtt.port', config))
    mqtt_username = jmespath.search('mqtt.username', config)
    mqtt_password = jmespath.search('mqtt.password', config)
    mqtt_topics = jmespath.search('mqtt.topics', config)
    mqtt_start_command = jmespath.search('mqtt.start_command', config)
    mqtt_stop_command = jmespath.search('mqtt.stop_command', config)

    client = paho.Client(clean_session=True)
    client.username_pw_set(mqtt_username, mqtt_password)
    client.will_set('iot/alerts/garage_watcher', payload='DISCONNECTED!', retain=True)
    client.on_disconnect = lambda client, userdata, rc: logger.warning('disconnected unexpectedly')
    client.tls_set()
    client.connect(mqtt_host, mqtt_port)

    # Setup callback for updates on subscribed topics
    for topic in mqtt_topics:
        client.subscribe(topic, qos=2)
        client.message_callback_add(topic, watcher_update)
    
    # Create Reminder
    reminder_units = jmespath.search('reminder.units', config)
    reminder_interval = jmespath.search('reminder.interval', config)
    reminder = Reminder(reminder_units, reminder_interval, alert)
    return client


def main():
    global args
    global logger

    parser = argparse.ArgumentParser()
    parser.add_argument("--dryrun", help="Dry Run for testing",
                        action='store_true')
    args = parser.parse_args()

    setup_logging()
    logger = logging.getLogger(__name__)
    logger.info('Garage Watcher started')

    if args.dryrun:
        logger.warning('Running in Dry Run mode')
        client = setup(config_path='debug_config.yml')
        logger.setLevel(logging.DEBUG)
    else:
        client = setup()
    client.loop_forever()

if __name__ == "__main__":
    main()
