FROM python:3.6
ADD . /code
WORKDIR /code
RUN pip install pipenv
RUN pipenv install --system
RUN adduser --uid 1000 --disabled-password --gecos '' wisdomwolf && chown -R wisdomwolf:wisdomwolf /code
CMD ["python", "app.py"]
